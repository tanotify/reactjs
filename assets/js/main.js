var compareList = [];
var tempClicks = 0;

var statistic = {

    init: function () {
        statistic.loadData();          // Main JS initialization
    },

    loadData: function () {
        $.ajax({
            url: "data.json",
            dataType: "json"
        }).done(function(data) {
            statistic.data = data;
            statistic.multiSelect();
        });
    },

    multiSelect: function () {
        $.each(statistic.data, function(i, elem) {
            $('#companiesList').append('<option data-id="' + elem.id + '" value="' + elem.title + '">' + elem.title + '</option>');
        });
        $('#companiesList').multiSelect({
            afterSelect: function(values){
                var tempItem = $.grep(statistic.data, function(elem) {
                    return elem.title === values.toString();
                });
                $('.graphic_data').addClass('dataLoaded');

                if (tempClicks < 8) {
                    tempClicks = tempClicks + 1;
                    compareList.push(tempItem);
                    statistic.generateCanvas(compareList);
                } else {
                    alert("Достигнут максимум");
                }
            },
            afterDeselect: function(values){
                var tempItem = values.toString();
                compareList.pop(tempItem);
            }
        });
    },

    generateCanvas: function(compareList) {
        var container = document.getElementById("graphic");
        var ctx  = container.getContext('2d');
        var colorPallete = ['#258702','#4a9f2a','#5fad44','#acdf9a','#d6dfd5','#d6dfd5','#d6dfd5','#d6dfd5'];

        var sumOffers = 0, sumShares = 0, sumLandings = 0, sumLeads = 0, sumPurchases = 0;
        //console.log(compareList[0][1]);

        // Посчитаем суммы всех параметров
        $.each(compareList, function( i, val) {
            sumOffers = sumOffers + val[0].metrics.offers;
            statistic.pushDiv('graphic_data-offers', sumOffers);
            sumShares = sumShares + val[0].metrics.shares;
            statistic.pushDiv('graphic_data-shares', sumShares);
            sumLandings = sumLandings + val[0].metrics.landings;
            statistic.pushDiv('graphic_data-landings', sumLandings);
            sumLeads = sumLeads + val[0].metrics.leads;
            statistic.pushDiv('graphic_data-leads', sumLeads);
            sumPurchases = sumPurchases + val[0].metrics.purchases;
            statistic.pushDiv('graphic_data-purchases', sumPurchases);
        });

        var persTotalOffers = 0, persTotalShares = 0, persTotalLandings = 0, persTotalLeads = 0, persTotalPurchases = 0;
        $.each(compareList, function( i, val) {
            var persOffers = (val[0].metrics.offers * 315) / (sumOffers);
            var persShares = (val[0].metrics.shares * 315) / (sumShares);
            var persLandings = (val[0].metrics.landings * 315) / (sumLandings);
            var persLeads = (val[0].metrics.leads * 315) / (sumLeads);
            var persPurchases = (val[0].metrics.purchases * 315) / (sumPurchases);

            ctx.beginPath();
            ctx.moveTo(persTotalOffers, 0);
            ctx.lineTo(persTotalShares, 79);
            ctx.lineTo(persTotalLandings, 158);
            ctx.lineTo(persTotalLeads, 237);
            ctx.lineTo(persTotalPurchases, 315);
            console.clear();
            console.log(persTotalOffers);

            persTotalOffers = persTotalOffers + persOffers;
            persTotalShares = persTotalShares + persShares;
            persTotalLandings = persTotalLandings + persLandings;
            persTotalLeads = persTotalLeads + persLeads;
            persTotalPurchases = persTotalPurchases + persPurchases;

            //persOffers = persOffers - (val[0].metrics.offers * 100) / (sumOffers);
            console.log(persTotalOffers);

            ctx.lineTo(persTotalOffers, 315);
            ctx.lineTo(persTotalShares, 237);
            ctx.lineTo(persTotalLandings, 158);
            ctx.lineTo(persTotalLeads, 79);
            ctx.lineTo(persTotalPurchases, 0);

            ctx.lineTo(0, 0);
            ctx.closePath();
            ctx.strokeStyle="#8ec779";
            ctx.stroke(); // *22
            ctx.fillStyle= colorPallete[i];
            console.log(ctx.fillStyle);
            ctx.fill(); // *14
            return persTotalOffers;
            return persTotalShares;
            return persTotalLandings;
            return persTotalLeads;
            return persTotalPurchases;
        });

        //console.log(sumOffers);


    },

    pushDiv: function(div, val) {
        $('.' + div).html(val);
    }

}

statistic.init();